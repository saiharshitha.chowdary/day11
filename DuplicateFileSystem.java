class Solution {
    public List<List<String>> findDuplicate(String[] paths) {
         HashMap<String,List<String>> map=new HashMap<>();
        for(int i=0;i<paths.length;i++){
            String[] p=paths[i].split(" ");
            for(int j=1;j<p.length;j++){
                String[] n =p[j].split("\\(");
                n[1]=n[1].substring(0,n[1].length()-1);
                List l=map.getOrDefault(n[1],new ArrayList<>());
                l.add(p[0]+"/"+n[0]);
                map.put(n[1],l);
            }
        }
        List<List<String>> list=new ArrayList<>();
        for(String k:map.keySet()){
            List l=map.get(k);
            if(l.size()<2){continue;}
            list.add(l);
        }
        return list;
    }
}
        
    