class Solution {
    public int maxProduct(int[] nums) {
         int len = nums.length;
        
        int[] left = new int[len];
        int[] right = new int[len];
        
        for(int i = 0, j = len - 1; i < len && j >= 0; i++, j--) {            
            left[i] = i == 0 || left[i-1] == 0 ? nums[i] : nums[i] * left[i-1]; 
            right[j] = j == len - 1 || right[j+1] == 0 ? nums[j] : nums[j] * right[j+1];
        }
        
        int maxProd = Integer.MIN_VALUE;        
        for(int i = 0; i < nums.length; i++) {
            int max = Math.max(left[i], right[i]);
            max = Math.max(max, nums[i]);
            
            maxProd = Math.max(maxProd, max);
        }
        
        return maxProd;
    }
}

        
    