class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
         if(head.next == null)
            return null;
        
        ListNode curr = head;
        int len=0;

        // Size of List
        while(curr != null) {
            curr = curr.next;
            len++;
        }
        if(n == len)
            return head.next;
        
        // Position of node before the node that has to be deleted
        int prev = len-n; 
        int count=1;
        curr = head;
        while(count < prev) {
            curr = curr.next;
            count++;
        }
        curr.next = curr.next.next;
        return head;
    }
}
        
    