class Solution {
    public ListNode swapPairs(ListNode head) {
         ListNode dummyList = new ListNode(0);
        ListNode s = dummyList, f = head;
        if(f== null || f.next == null){
            return f;
        }
        
        while(f!= null && f.next != null){
            s.next = new ListNode(f.next.val);
            s.next.next = new ListNode(f.val);
            s = s.next.next;
            f = f.next.next;
        }
        if(f!= null && f.next == null ){
            s.next = new ListNode(f.val);
        }
        return dummyList.next;
    }
}