class Solution {
    public int compress(char[] chars) {
         int lo=0, hi=0, index=0;
        while(hi<chars.length){
            while(hi<chars.length-1 && chars[hi+1]==chars[lo]) hi++;
            int l=hi+1-lo;
            chars[index++]=chars[lo];
            hi++;
            lo=hi;
            if (l!=1){
                int i=index;
                while(l>0){
                    chars[i++]=(char)(l%10+'0');
                    l/=10;
                }
                int left=index, right=i-1;
                while(left<right){
                    char temp=chars[left];
                    chars[left++]=chars[right];
                    chars[right--]=temp;
                }
                index=i;
            }
        }
        return index;
    }
}
        
    
