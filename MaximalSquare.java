class Solution {
    public int maximalSquare(char[][] matrix) {
         int R = matrix.length;
        int C = R > 0 ? matrix[0].length : 0;
        
        int[][] dp = new int[R + 1][C + 1];
        int max = 0;
        
        for (int i = 1; i <= R; i++) {
            for (int j = 1; j <= C; j++) {
                if (matrix[i - 1][j - 1] == '1'){
                    dp[i][j] = 1 + Math.min(Math.min(dp[i][j - 1], dp[i - 1][j]), dp[i - 1][j - 1]);
                    max = Math.max(max, dp[i][j]);
                }
            }
        }
        
        return max * max;
    }
}
    