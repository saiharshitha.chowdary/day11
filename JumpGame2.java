class Solution {
    public int jump(int[] nums) {
         if (nums.length < 2) return 0;
        int curPos = 0;
        int maxPos = 0;
        int jumps = 0; // keep track of the jumps
        for (int i = 0; i < nums.length; i++) {
            maxPos = Math.max(maxPos, i + nums[i]);
            if (i == curPos) {
                // basically will make a jump
                jumps++;
                // and update the maxCurPos 
                curPos = maxPos;
            }
            if (curPos >= nums.length-1) return jumps;
        }
        return -1;
    }
}
